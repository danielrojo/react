const reducer = (state = {
    sw: {
        heroes: [],
        next: 'https://swapi.dev/api/people/'
    },
    beers: {
        range: [0, 6]
    },
    heroes: {
        newHero: {
            name: '',
            description: ''
        },
        heroes: [
            { name: "Batman", description: "Dak knight" },
            { name: "Superman", description: "Man of Steel" },
            { name: "Spiderman", description: "spidy" }]
    },
    trivial:{
        cards:[],
        points: 0
    }
}, action) => {
    switch (action.type) {
        case "UPDATE_CARDS":
            state.trivial.cards = action.payload.cards;
            return state;
        case "UPDATE_HEROES":
            state.sw.heroes = action.payload.heroes;
            return state;
        case "UPDATE_NEXT":
            state.sw.next = action.payload.next;
            return state;
        case "UPDATE_RANGE":
            state.beers.range = action.payload.range;
            return state;
        case "ADD_HERO":
            state.heroes.heroes = [...state.heroes.heroes, action.payload.hero];
            return state;
        case "UPDATE_NEW_HERO":
            state.heroes.newHero = action.payload.hero;
            return state;

        default:
            return state;
    }
};

export default reducer;