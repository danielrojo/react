import React from 'react';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Calculator from './components/calculator/calculator';
import 'bootstrap/dist/css/bootstrap.min.css';
import Heroes from './containers/heroescontainer';
import Apod from './components/apod/apod';
import Fbeers from './containers/beercontainer';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { render } from "react-dom";
// import SwComponent from './components/sw/swcomponent';
import { createStore } from "redux";
import { Provider } from "react-redux";

import reducer from "./reducers/reducer";
import SwComponent from "./containers/swcontainer";
import Trivial from "./containers/trivialcontainer";

const store = createStore(reducer);

render(
  <React.StrictMode>
    <Provider store={store}>
      <Router>
        <div>
          <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
              aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavId">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li>
                  <Link className="nav-link" to="/calculator">Calculadora</Link>
                </li>
                <li>
                  <Link className="nav-link" to="/heroes">Héroes</Link>
                </li>
                <li>
                  <Link className="nav-link" to="/apod">Apod</Link>
                </li>
                <li>
                  <Link className="nav-link" to="/beers">Cervezas</Link>
                </li>
                <li>
                  <Link className="nav-link" to="/sw">Star Wars</Link>
                </li>
                <li>
                  <Link className="nav-link" to="/trivial">Trivial</Link>
                </li>
              </ul>
            </div>
          </nav>


          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/calculator">
              <Calculator />
            </Route>
            <Route path="/heroes">
              <Heroes />
            </Route>
            <Route path="/apod">
              <Apod />
            </Route>
            <Route path="/beers">
              <Fbeers />
            </Route>
            <Route path="/sw">
              <SwComponent />
            </Route>
            <Route path="/trivial">
              <Trivial />
            </Route>
            <Route path="/">
              <Trivial />
            </Route>
            <Route path="*">
              <h1>No está esta ruta</h1>
            </Route>
          </Switch>
        </div>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
