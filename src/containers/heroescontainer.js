import Heroes from "../components/heroes/heroes";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    heroes: state.heroes.heroes,
    newHero: state.heroes.newHero
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addHeroToRedux: hero => {
      dispatch({ type: "ADD_HERO", payload: { hero: hero } });
    },
    updateNewHeroToRedux: newHero => {
      dispatch({ type: "UPDATE_NEW_HERO", payload: { hero: newHero } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Heroes);