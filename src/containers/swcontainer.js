import SwComponent from "../components/sw/swcomponent";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    heroes: state.sw.heroes,
    next: state.sw.next
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateHeroes: myHeroes => {
      dispatch({ type: "UPDATE_HEROES", payload: { heroes: myHeroes } });
    },
    updateNext: next => {
        dispatch({ type: "UPDATE_NEXT", payload: { next: next } });
      }
  
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SwComponent);