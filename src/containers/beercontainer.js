import Fbeers from "../components/beers/fbeers";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    range: state.beers.range
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateRange: myRange => {
      dispatch({ type: "UPDATE_RANGE", payload: { range: myRange } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Fbeers);