import Trivial from "../components/trivial/trivial";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    cards: state.trivial.cards
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCards: myCards => {
      dispatch({ type: "UPDATE_CARDS", payload: { cards: myCards } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Trivial);