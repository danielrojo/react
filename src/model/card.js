export class Card {
    constructor(data) {
        if(data) {
            this.question = data.question;
            this.answers = [...data.incorrect_answers, data.correct_answer];
            this.rightAnswer = data.correct_answer;   
            this.answered = false;
            this.userAnswer = ''
            
        } else {
            this.question = '';
            this.answers = [];
            this.rightAnswer = '';    
            this.answered = false;
            this.userAnswer = ''
            this.index = -1
        }
    }
    
}

export default Card
