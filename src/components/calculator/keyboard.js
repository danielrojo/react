import React, { Component, Fragment } from 'react'

export default class Keyboard extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    handleClick(value) {
        this.props.signal(value);
    }

    render() {
        return (
            <Fragment>
                <tr>
                    <td><input type="button" value="1" onClick={() => { this.handleClick(1) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="2" onClick={() => { this.handleClick(2) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="3" onClick={() => { this.handleClick(3) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="/" onClick={() => { this.handleClick('/') }} className="btn btn-primary btn-lg btn-block" /> </td>
                </tr>
                <tr>
                    <td><input type="button" value="4" onClick={() => { this.handleClick(4) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="5" onClick={() => { this.handleClick(5) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="6" onClick={() => { this.handleClick(6) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="-" onClick={() => { this.handleClick('-') }} className="btn btn-primary btn-lg btn-block" /> </td>
                </tr>
                <tr>
                    <td><input type="button" value="7" onClick={() => { this.handleClick(7) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="8" onClick={() => { this.handleClick(8) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="9" onClick={() => { this.handleClick(9) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="+" onClick={() => { this.handleClick('+') }} className="btn btn-primary btn-lg btn-block" /> </td>
                </tr>
                <tr>
                    <td><input type="button" value="." onClick={() => { this.handleClick('.') }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="0" onClick={() => { this.handleClick(0) }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="=" onClick={() => { this.handleClick('=') }} className="btn btn-primary btn-lg btn-block" /> </td>
                    <td><input type="button" value="x" onClick={() => { this.handleClick('x') }} className="btn btn-primary btn-lg btn-block" /> </td>
                </tr>

            </Fragment>
        )
    }
}
