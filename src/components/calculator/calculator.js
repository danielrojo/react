import React, { Component } from 'react';
import './calculator.css';
import Clear from './clear';
import Display from './display';
import Keyboard from './keyboard';
import Container from 'react-bootstrap/Container'

const INIT_STATE = 0;
const FIRST_FIGURE_STATE = 1;
const SECOND_FIGURE_STATE = 2;
const RESULT_STATE = 3;

export default class Calculator extends Component {

    calculatorState = INIT_STATE;
    firstFigure = 0;
    secondFigure = 0;
    result = 0;
    operator = '';

    constructor(props) {
        super(props)

        this.state = {
            display: ''
        }
    }

    handleFigure(number) {
        switch (this.calculatorState) {
            case INIT_STATE:
                this.firstFigure = number;
                this.calculatorState = FIRST_FIGURE_STATE;
                this.setState({ display: this.state.display + String(number) });
                break;
            case FIRST_FIGURE_STATE:
                this.firstFigure = this.firstFigure * 10 + number;
                this.setState({ display: this.state.display + String(number) });
                break;
            case SECOND_FIGURE_STATE:
                this.secondFigure = this.secondFigure * 10 + number;
                this.setState({ display: this.state.display + String(number) });
                break;
            case RESULT_STATE:
                this.firstFigure = number;
                this.secondFigure = 0;
                this.result = 0;
                this.operator = '';
                this.calculatorState = FIRST_FIGURE_STATE;
                this.setState({ display: String(number) });
                break;

            default:
                break;
        }
    }

    handleSymbol(symbol) {
        switch (this.calculatorState) {
            case INIT_STATE:

                break;
            case FIRST_FIGURE_STATE:
                if (this.isOperator(symbol)) {
                    this.operator = symbol;
                    this.calculatorState = SECOND_FIGURE_STATE;
                    this.setState({ display: this.state.display + symbol });
                }
                break;
            case SECOND_FIGURE_STATE:
                if (symbol === '=') {
                    this.result = this.resolve();
                    this.setState({ display: this.state.display + symbol + String(this.result) });
                    this.calculatorState = RESULT_STATE;
                }
                break;
            case RESULT_STATE:
                if (this.isOperator(symbol)) {
                    this.firstFigure = this.result;
                    this.operator = symbol;
                    this.secondFigure = 0;
                    this.result = 0;
                    this.calculatorState = SECOND_FIGURE_STATE;
                    this.setState({ display: String(this.firstFigure) + this.operator });
                }
                break;

            default:
                break;
        }
    }

    resolve() {
        switch (this.operator) {
            case '+':
                return this.firstFigure + this.secondFigure;
            case '-':
                return this.firstFigure - this.secondFigure;
            case 'x':
                return this.firstFigure * this.secondFigure;
            case '/':
                return this.firstFigure / this.secondFigure;

            default:
                break;
        }
    }

    isOperator(symbol) {
        return (symbol === '+' || symbol === '-' || symbol === 'x' || symbol === '/')
    }

    handleClick(value) {
        if (typeof value === 'number') {
            this.handleFigure(value);
        } else if (typeof value === 'string') {
            this.handleSymbol(value);
        }
    }

    clear() {
        this.firstFigure = 0;
        this.secondFigure = 0;
        this.operator = '';
        this.result = 0;
        this.calculatorState = INIT_STATE;
        this.setState({display: ''});
    }

    render() {
        return (
            <Container className="mt-5">
                <table border="1">
                    <tbody>
                        <tr>
                            <Display text={this.state.display} />
                            <td><Clear event={()=>{this.clear()}}/></td>
                        </tr>
                        <Keyboard signal={(myValue) => { this.handleClick(myValue) }} />
                    </tbody>
                </table>

            </Container>
        )
    }
}
