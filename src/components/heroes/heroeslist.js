import React, { Component } from 'react'
import ListGroup from 'react-bootstrap/ListGroup';

export default class HeroesList extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const listHeroes = this.props.heroes.map(
            (hero, index) => <ListGroup.Item key={index}><strong>{hero.name}</strong> <br /> {hero.description} </ListGroup.Item>)
        return (
            <ListGroup>
                {listHeroes}
            </ListGroup>
        )
    }
}
