import React, { Component } from 'react'

export default class HeroesForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            newHero: this.props.currentHero,
            showField: 'alert alert-danger alert-dismissible fade show'
        }
    }

    addHero() {
        this.props.newHero(this.state.newHero);
        this.setState({ newHero: { name: '', description: '' } })
    }

    handleChange(event) {
        let myHero = this.state.newHero;
        if (event.target.name === 'heroName') {
            myHero.name = event.target.value;
        } else if (event.target.name === 'heroDescription') {
            myHero.description = event.target.value;
        }
        this.setState({ newHero: myHero });
        this.props.updateHero(myHero);

        if (event.target.validity.valid) {
            this.setState({ showField: 'alert alert-danger alert-dismissible fade' });
            this.button = <button type="button" className="btn btn-primary mt-2 mb-3" onClick={() => { this.addHero() }}  >Añade</button>

        } else {
            this.setState({ showField: 'alert alert-danger alert-dismissible fade show' })
            this.button = <button type="button" className="btn btn-primary mt-2 mb-3" onClick={() => { this.addHero() }} disabled>Añade</button>


        }

    }

    button = <button type="button" className="btn btn-primary mt-2 mb-3" onClick={() => { this.addHero() }} disabled>Añade</button>


    render() {
        return (
            <div className="form-group">
                <label for="">Nuevo héroe</label>
                <input type="text" className="form-control" name="heroName" id="" aria-describedby="helpId" placeholder="" value={this.state.newHero.name} onChange={(event) => { this.handleChange(event) }} required={true} />
                <div className={this.state.showField} role="alert">
                    
                    <strong>El nombre está mal</strong>
                </div>
                <input type="text" className="form-control" name="heroDescription" id="" aria-describedby="helpId" placeholder="" value={this.state.newHero.description} onChange={(event) => { this.handleChange(event) }} />
                {this.button}
            </div>
        )
    }
}
