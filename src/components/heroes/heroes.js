import React, { Component } from 'react';
import Container from 'react-bootstrap/esm/Container';
import HeroesForm from './heroesform';
import HeroesList from './heroeslist';

export default class Heroes extends Component {

    constructor(props) {
        super(props)

        this.state = {
            heroes: this.props.heroes
        }
    }

    handleChange(event) {
        let myHero = this.state.newHero;
        if(event.target.name === 'heroName'){
            myHero.name = event.target.value;
        } else if(event.target.name === 'heroDescription'){
            myHero.description = event.target.value;
        }
        this.setState({ newHero: myHero });
    }

    addHero(hero) {
        this.setState({ heroes: [...this.state.heroes, hero]})
        this.props.addHeroToRedux(hero);
    }

    updateHero(hero){
        this.props.updateNewHeroToRedux(hero)
    }

    render() {

        return (
            <Container>
                <HeroesForm newHero = {(myHero)=>{this.addHero(myHero)}} currentHero={this.props.newHero} updateHero={(hero)=>{this.updateHero(hero)}}/>
                <HeroesList heroes={this.state.heroes}></HeroesList>
            </Container>
        )
    }
}
