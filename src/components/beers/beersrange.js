import React, {useState} from 'react';
import Slider from '@material-ui/core/Slider';

function BeersRange(props) {

    const [range, setRange] = useState(props.range);

    const handleChange = (event, value) => {
        console.log(value);
        props.rangeSignal(value);
        setRange(value);
    };

    return (
        <Slider
        value={range}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        min={0}
        max={50}
        step={0.1}
    />)

}


export default BeersRange

