import React, { useState, useEffect } from 'react';
import BeersList from './beerslist';
import BeersRange from './beersrange';

function Fbeers(props) {

    const [range, setRange] = useState(props.range)

    const handleChange = (value) => {
        console.log(value);
        setRange(value);
        props.updateRange(value);
    };

    useEffect(() => {
        return () => {
            console.log('componentWillUnmount()');
        }
    }, [])

    return (
        <div class="container">
            <BeersRange rangeSignal={handleChange} range={range}></BeersRange>
            <BeersList myRange={range}></BeersList>
        </div>
    )


}

export default Fbeers

