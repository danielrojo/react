import React, { Component } from 'react';
import Slider from '@material-ui/core/Slider';

export default class Beers extends Component {

    constructor(props) {
        super(props)

        this.state = {
            beers: [],
            selectedBeers: [],
            range: [0,5]
        }
    }

    render() {
        const beers = this.state.selectedBeers.map((beer, index) => {
            return (
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">{beer.name}</h4>
                            <img class="card-img-top mx-auto d-block" src={beer.image_url} style={{ width: '45px' }} alt="" />
                            <p class="card-text">{beer.tagline}</p>
                            <p class="card-text">{beer.abv}</p>
                        </div>
                    </div>
                </div>
            )
        });
        return (
            <div class="container">
                <Slider
                    value={this.state.range}
                    onChange={(event,value)=>{this.handleChange(value)}}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    min={0}
                    max={50}
                    step={0.1}
                />
                <div class="row">
                    {beers}
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.doRequest()
    }

    handleChange = (value) => {
        console.log(value);
        this.setState({
            range: value,
            selectedBeers: this.state.beers.filter((beer)=>{return beer.abv >= this.state.range[0] && beer.abv <= this.state.range[1]})});
      };

    doRequest() {
        let url = 'https://api.punkapi.com/v2/beers';
        fetch(url)
            .then((response) => { return response.json() })
            .then((data) => {
                console.log(data);
                this.setState({ beers: data, selectedBeers: data });
            })
    }
}
