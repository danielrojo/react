import React, {useState, useEffect} from 'react'

function BeersList(props) {

    const [query] = useState('https://api.punkapi.com/v2/beers');
    const [data, setData] = useState([]);
    const [beers, setBeers] = useState([]);

    const updateBeersRange =  () => {
        setBeers(data.filter((beer) => { return beer.abv >= props.myRange[0] && beer.abv <= props.myRange[1] }));
    }

    const myFetch = () => {    
        const fetchData = async () => {
            console.log('Fetching...');
            const response = await fetch(
                query
            );
            const data = await response.json();
            console.log(data);
            setData(data);
            setBeers(data);
        };

        fetchData();
    };

    useEffect(myFetch, [query]);
    useEffect(updateBeersRange, [props.myRange, data])


    const MyBeers = beers.map((beer, index) => {
        return (
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{beer.name}</h4>
                        <img class="card-img-top mx-auto d-block" src={beer.image_url} style={{ width: '45px' }} alt="" />
                        <p class="card-text">{beer.tagline}</p>
                        <p class="card-text">{beer.abv}</p>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div class="row">
            {MyBeers}
        </div>
    )
}


export default BeersList

