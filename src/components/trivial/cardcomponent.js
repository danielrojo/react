import React, {useState} from 'react'

function CardComponent(props) {
    const [card, setCard] = useState(props.card);
    const [answered, setAnswered] = useState(false)

    const handleClick = (answer) => {
        let temp = card;
        temp.answered = true;
        temp.userAnswer = answer;
        console.log('click: ' + (JSON.stringify(temp)));
        setCard(temp);
        setAnswered(true);
        props.rightAnswer(answer === card.rightAnswer);
    }

    const getClass = (answer) => {
        if(answer === card.rightAnswer) return 'btn btn-lg btn-block btn-success';
        if(answer === card.userAnswer) return  'btn btn-lg btn-block btn-danger';
        return 'btn btn-lg btn-block btn-secondary'
    }

    let buttons = <div></div>
    if(answered && card.answers){
        buttons = card.answers.map((answer, index) => <button type="button" class={getClass(answer)} key={index} onClick={()=>handleClick(answer)} disabled>{answer}</button>);

    }else if(card.answers) {
        buttons = card.answers.map((answer, index) => <button type="button" class="btn btn-lg btn-block btn-primary" key={index} onClick={()=>handleClick(answer)}>{answer}</button>);
    }

    const question = React.createElement("h4", { dangerouslySetInnerHTML: { __html: card.question } });
    return (
        <div class="card">
            <div class="card-body">
                {question}
                {buttons}
            </div>
        </div>)
}

export default CardComponent
