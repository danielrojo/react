import React, { useState, useEffect } from 'react'
import Card from '../../model/card';
import CardComponent from './cardcomponent';

function Trivial(props) {
    const [query, setQuery] = useState('https://opentdb.com/api.php?amount=10');
    const [data, setData] = useState({});
    const [cards, setCards] = useState([]);
    const [points, setPoints] = useState(0);

    const myFetch = () => {
        const fetchData = async () => {
            console.log('Fetching...');
            const response = await fetch(
                query
            );
            const temp = await response.json();
            setData(temp);
            let tempCards = []
            for (const cardData of temp.results) {
                console.log('cardData: ' + JSON.stringify(cardData));
                tempCards = [...tempCards, new Card(cardData)];
            }
            setCards([...cards, ...tempCards]);
            props.updateCards([...cards, ...tempCards]);
        };

        fetchData();
    };

    const fetchFromRedux = () => {
        setCards(props.cards);
    }

    useEffect(myFetch, [query]);
    useEffect(fetchFromRedux, [props.cards]);

    const handleAnswer = (e)=>{
        if(e){
            setPoints(points + 2);
        }else {
            setPoints(points - 1);
        }
    }

    const cardArray = cards.map((card, index) =>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <CardComponent card={card} key={index} rightAnswer={(e)=>handleAnswer(e)}/>
        </div>)

    let content = <div></div>;

    if (cards !== undefined) {
        content = cardArray;
    }

    return (

        <div class="container">
            <span class="badge badge-primary">{points}</span>
            <div class="row">
                {content}
            </div>
        </div>
    )
}

export default Trivial
