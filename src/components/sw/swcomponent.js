import React, { useState, useEffect } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component';

function SwComponent({heroes, next, updateHeroes, updateNext}) {

    const [query, setQuery] = useState(next);
    const [data, setData] = useState([]);
    const [people, setPeople] = useState(heroes);

    const myFetch = () => {
        const fetchData = async () => {
            console.log('Fetching...');
            const response = await fetch(
                query
            );
            const data = await response.json();
            setData(data);
            setPeople([...people, ...data.results]); 
            updateHeroes(people);
            updateNext(data.next);
        };

        fetchData();
    };

    const fetchData = () => {
        setQuery(data.next);
    }

    useEffect(myFetch, [query]);

    const peopleList = people.map((person, index) => {
        return (
            <div className="col-sm-12 col-md-6 col-lg-4" key={index}>
                <div className="card">
                    <div className="card-body">
                        <h4 className="card-title">{person.name}</h4>
                        <p className="card-text">Altura: {person.height}</p>
                        <p className="card-text">Peso: {person.mass}</p>
                    </div>
                </div>
            </div>)
    });
    return (
        <div className="container">
                <div className="row">
                    {peopleList}
                </div>
                <button type="button" class="btn btn-primary"onClick={fetchData}>más</button>
        </div>
    )
}

export default SwComponent
