import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import moment from 'moment';


export default class DateApod extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            startDate: new Date()
        }
    }

    
    render() {
        return (
            <DatePicker selected={this.state.startDate} onChange={(date) => {
                this.setState({startDate: date});
                this.props.selectedDate(moment(date).format('YYYY-MM-DD'))}} />

        )
    }
}
