import React, { Component } from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import ReactPlayer from 'react-player';


export default class DisplayApod extends Component {

    didMount = false;

    constructor(props) {
        super(props)

        this.state = {
            apod : {},
            status: 'idle'
        }
    }

    doRequest(date) {
        if (this.state.status !== 'idle') return;
        let url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&thumbs=true'
        if (!date) {

        } else {
            url = url + '&date=' + date;
        }
        this.setState({status: 'fetching'});

        fetch(url)
            .then((response) => { return response.json() })
            .then((data) => {
                console.log(data);
                this.setState({ apod: data });
                this.setState({status: 'fetched'});
            })
    }


    componentDidUpdate(prevProps, prevState) {
        console.log(this.props.myDate+':'+prevProps.myDate);
        if(this.props.myDate !== prevProps.myDate || this.didMount){
            this.doRequest(this.props.myDate);
            this.didMount = false;
        }
    }

    componentDidMount() {
        this.didMount = true;
        this.doRequest(this.props.myDate);
        
    }
    
    

    render() {

        let contentTag = <div></div>;
        let button = <div></div>;

        if (this.state.apod.media_type === 'image') {
            contentTag = <img src={this.state.apod.url} className="mx-auto d-block" alt=""></img>
            button = <Button variant="primary" href={this.state.apod.hdurl}>Imagen HD</Button>;
        } else if (this.state.apod.media_type === 'video') {
            contentTag = <ReactPlayer url={this.state.apod.url} className="mx-auto d-block" />
            button = <></>;
        }
        return (
            <Jumbotron className="mt-3">
                <h1>{this.state.apod.title}</h1>
                {contentTag}
                <p className="mt-2">{this.state.apod.explanation}</p>
                {button}
            </Jumbotron>
        )
    }
}
