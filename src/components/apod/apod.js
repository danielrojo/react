import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import DateApod from './dateapod';
import DisplayApod from './displayapod';

export default class Apod extends Component {
    constructor(props) {
        super(props)

        this.state = {
            date: ''
        }
    }

    componentDidMount() {
        console.log('componentDidMount()');
        this.setState({date: moment(new Date()).format('YYYY-MM-DD')});
    }
    


    render() {
        
        let content = <></>;


        const contentPage =
            <div className="container">
                <DateApod selectedDate={(dateString)=>{this.setState({date: dateString})}}></DateApod>
                <DisplayApod myDate={this.state.date}></DisplayApod>
            </div>


        content = contentPage;

        return (
            <div>
                {content}
            </div>
        )
    }
}
